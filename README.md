# Template projet Python

Ce dépot est un template de projet Python avec rapport LateX sur GitLab prenant en
charge :

- La configuration de Gitlab
- Le gitignore
- La mise en place de l'environnement virtuel
- La mise en place de CI/CD
- Une architecture basique
- Des tests automatisés avec Pytest et tox
- Un template de rapport Latex

## Créer un environnement virtuel

Dans le dossier du projet, exécuter les commandes suivantes :

- Pour créer l'environnement virtuel

```commandline
pip install virtualenv
virtulenv venv
```

- Pour activer l'environnement virtuel

```commandline
source venv/bin/activate
```

- Pour désactiver l'environnement virtuel

```commandline
source venv/bin/deactivate
```

- Pour installer un nouveau package :

```commandline
pip install <nom-package>
```

- Pour installer tous les requirements (le fichier actuel contient le packages nécessaires pour
  le template) :

```commandline
pip install -r requirements.txt
```

- Pour ajouter de nouveaux paquets au fichier requirements.txt (celui-ci est écrasé) :

```commandline
pip freeze > requirements.txt
```

## Configurer le projet Gitlab

### General > Visibility, project features, permissions :

Modules nécessaires :

- Issues
- Repository
    - Merge Request
    - CI/CD

### Merge requests:

- merge method -> merge commit
- squash commit when merging -> Do not allow

### Repository > Branch Default:

- default branch -> main
- Auto-close referenced issues on default branch -> Oui

### Repository > Push rules (Only premium Gitlab):

Require expression in commit messages ->
`(?:build|Build|upgrade|Upgrade|ci|doc|docs|Docs|feat|New|fix|fixes|Fix|perf|perfs|Update|refactor|style|test|tests |breaking|Breaking|merge)(?:\([a-z0-9-]*\))?: .+`

### Repository > Protected Branch :

branche main :

- Allowed to merge -> Developers + Maintainers
- Allowed to push -> Maintainers
- Allowed to force push -> Non

### CI/CD > Runner :

Enable instance runners for this project -> Oui

### Optionnel : Ajouter des labels
Ajouter des labels pour les issues et merge requests.
Labels conseillés : To do, Urgent, Stand by, Fix, Doc.

## Gitignore

Le fichier [`.gitignore`](./.gitignore) contient tous les fichiers et dossiers habituellement
ignorés par git dans un projet et Latex. Notamment :

- le cache python
- les résultats de tests
- les environnements virtuels
- les fichiers auxiliaires Latex
- les fichiers de configuration des principaux IDE

## setup.py

Le nom de projet est à modifier dans le fichier [`setup.py`](./setup.py).

## Test

Les tests utilisent le framework [Pytest](https://docs.pytest.org/en/7.1.x/) et doivent être
placés dans le dossier [`tests`](./tests).

Les tests sont automatisés avec [tox](https://tox.wiki/en/latest/index.html) qui est configuré
dans le fichier [`tox.ini`](./tox.ini).
Pour exécuter les tests, exécuter la commande :

```commandline
tox
```

L'analyse de couverture des tests est effectuée
par [pytest-cov](https://pytest-cov.readthedocs.io/en/latest/). Cela génère un fichier pour
chaque fichier source indiquant quelles lignes sont testées.

## CI/CD

La pipeline de Ci/CD est définie dans le fichier [`.gitlab-ci.yml`](./.gitlab-ci.yml).

Elle st composée de deux étapes :
- L'exécution de tests unitaires avec [tox](https://tox.wiki/en/latest/index.html)
- L'analyse du code avec [mypy](https://mypy.readthedocs.io/en/stable/index.html) (Analyse statique des types), [pycodestyle](https://pycodestyle.pycqa.org/en/latest/) (Analyse du coding style) et [pydocstyle](http://www.pydocstyle.org/en/stable/) (Analyse des docstring)

## Rapport Latex
Un template de rapport Latex est proposé [ici](./docs/main.tex).
Il utilise une bibliographie biblatex (actuellement fichier [Mybib.bib](./docs/Mybib.bib)).