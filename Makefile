install:
	pip install --upgrade pip &&\
		pip install -r requirements.txt

doc:
	cd docs/ &&\
    	lualatex -interaction=nonstopmode main.tex &&\
    	biber main &&\
    	lualatex -interaction=nonstopmode main.tex &&\
    	lualatex -interaction=nonstopmode main.tex &&\
  		mv main.pdf ../main.pdf &&\
  		rm -f *.aux *.bbl *.bcf *.blg *.log *.out *.xml *.toc &&\
  		cd ../

test:
	tox

lint:
	mypy --ignore-missing-imports **/*.py &&\
		pycodestyle --max-line-length=100 **/*.py &&\
    	pydocstyle **/*.py

check: lint test

all: install check doc