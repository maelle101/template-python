from setuptools import setup, find_packages

with open("README.md", "r") as f:
    long_description = f.read()

setup(name='Nom de projet', packages=find_packages(include=['src', 'tests']),
      long_description=long_description)
