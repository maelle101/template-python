"""Testing module."""
import pytest

from src import main


class TestMain:
    """Here is an example of class docstring.

    Methods
    -------
    test_name() -> None
        Description of method
    """

    def test_name(self) -> None:
        """Test function name."""
        assert main.printName("Jack") == "My name is Jack"
        with pytest.raises(ValueError):
            main.printName(3)  # type: ignore
