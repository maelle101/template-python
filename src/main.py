"""Here is an example of module docstring."""


def printName(name: str) -> str:
    """Describe function here.

    Parameters
    ----------
    name: str
        Description of the parameter

    Returns
    -------
    str
        Description of function return

    Raises
    ------
    ValueError
        Argument provided has wrong type
    """
    if type(name) is not str:
        raise ValueError(f"Wrong type, expected str, got {type(name)}")
    return f"My name is {name}"


if __name__ == "__main__":
    print("Hello World! " + printName('John'))
